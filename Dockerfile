FROM golang
RUN go install github.com/pressly/goose/cmd/goose@latest
RUN export PATH="$GOPATH:$PATH"
COPY db db
CMD ["goose", "-dir", "db/migrations/", "up"]
