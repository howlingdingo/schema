This is the db schemas that are used by the microservices.

They are managed by GOOSE - a go migration utility that is language agnostic.
https://github.com/pressly/goose

This is important as one of the ideas of this project will be to compare alternate implementation technologies for the microservices.

Steps for setting up postgres on ubuntu:
$ sudo apt install postgresql postgresql-contrib psql

$ sudo vim /etc/postgresql/12/main/pg_hba.conf

# IPv4 local connections:
host    all             all             127.0.0.1/32            trust
# --> Docker
host    all             all             172.17.0.0/16           trust

$ sudo service postgresql restart


Test install (optional)
$ sudo -i -u postgres
$ psql
% \q
$ exit

Add current user, connect and get connection info
$ sudo -u postgres createuser -s `whoami`
$ createdb  `whoami`
$ psql
% \c
% \q

https://golang.org/doc/install

$ go get -u github.com/pressly/goose/cmd/goose

Running goose:
$ ~/go/bin/goose create foo sql
