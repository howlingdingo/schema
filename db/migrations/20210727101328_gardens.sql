-- +goose Up
-- +goose StatementBegin
SELECT 'up SQL query';

CREATE TABLE gardens (
  id SERIAL PRIMARY KEY,
  external_id uuid UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  name VARCHAR NOT NULL,
  description TEXT,
  owner uuid
);

insert into gardens ( created_at, name, description, owner)
 select now(), 'back garden', 'back out front', external_id from USERS where name = 'asdf';

insert into gardens ( created_at, name, description, owner)
 select now(), 'front garden', 'front out front', external_id from USERS where name = 'asdf';



-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
drop table gardens;
-- +goose StatementEnd
