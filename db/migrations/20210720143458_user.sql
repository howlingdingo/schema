-- +goose Up
-- +goose StatementBegin
SELECT 'up SQL query';


CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  external_id uuid UNIQUE NOT NULL DEFAULT uuid_generate_v4(),
  created_at timestamp NULL DEFAULT NULL,
  updated_at timestamp NULL DEFAULT NULL,
  deleted_at timestamp NULL DEFAULT NULL,
  email VARCHAR NOT NULL,
  name VARCHAR NOT NULL,
  password VARCHAR DEFAULT NULL,
  accepted boolean DEFAULT FALSE,
  enabled boolean DEFAULT TRUE,
  CONSTRAINT unique_email UNIQUE(email)
);

insert into users ( created_at, email, name, password, accepted, enabled)
values (now(), 'asdf@asdf.com.au', 'asdf', 'asdf', true, true),
       (now(), 'qwer@asdf.com.au', 'qwer', 'qwer', true, true),
       (now(), 'uiop@asdf.com.au', 'uiop', 'uiop', true, false),
       (now(), 'zxcv@asdf.com.au', 'zxcv', 'zxcv', false, true),
       (now(), 'hjkl@qwer.com.au', 'hjkl', 'hjkl', false, false);

-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
DROP TABLE users;
-- +goose StatementEnd
